// 该文件用于创建Vuex找那个最为核心的store
// 引入Vue
import Vue from 'vue'

// 引入Vuex
import Vuex from 'vuex'

// 使用Vuex插件
Vue.use(Vuex)

// 准备actions--用于响应组件中的动作
const actions = {
    add(context,value){
        // console.log("我是actions，add方法被调用了",context,value)
        context.commit('ADD',value)
    },
    subtract(context,value){
        // console.log("我是actions，subtract方法被调用了",context,value)
        context.commit('SUBTRACT',value)
    },
    addOdd(context,value){
        // console.log("我是actions，addOdd方法被调用了",context,value)
        // context.commit('ADDODD',value)
        if(context.state.sum % 2){
            context.commit('ADD',value)
        }
    },
    addWait(context,value){
        // console.log("我是actions，addWait方法被调用了",context,value)
        // context.commit('ADDWAIT',value)
        setTimeout(()=>{
            context.commit('ADD',value)
        },1500)
    },
}

// 准备mutations--用于操作数据
const mutations = {
    ADD(state,value){
        // console.log("我是mutations，ADD方法被调用了",context,value)
        console.log(state)
        state.sum += value
    },
    SUBTRACT(state,value){
        // console.log("我是mutations，SUBTRACT方法被调用了",context,value)
        state.sum -= value
    },
}

// 准备state--用于存储数据
const state = {
    sum:0,
    school:'华南理工大学',
    subject:"数学"
}

//准备getters，一般用于将state中的数据进行加工
const getters = {
    bigSum(state){
        return state.sum * 10
    }
}

export default new Vuex.Store({
    actions,
    mutations,
    state,
    getters
})