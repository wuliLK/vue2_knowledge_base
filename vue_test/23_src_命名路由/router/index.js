//改组件专门用于创建整个应用的路由器
import vueRouter from 'vue-router'

//引入组件
import About from '../pages/About.vue'
import Home from '../pages/Home.vue'
import News from '../pages/News.vue'
import Message from '../pages/Message.vue'
import Detail from '../pages/Detail.vue'

export default new vueRouter({
    routes:[
        {
            path:'/about',
            component:About
        },
        {
            path:'/home',
            component:Home,
            children:[
                {
                    // 命名路由，可以直接通过这么name属性值直接找到这条路由
                    name:"news",
                    path:'news',
                    component:News
                },
                {
                    path:'message',
                    component:Message,
                    children:[
                        {
                            name:'zf',
                            path:'detail',
                            component:Detail,

                        }
                    ]
                }
            ]
        }
    ]

    
})