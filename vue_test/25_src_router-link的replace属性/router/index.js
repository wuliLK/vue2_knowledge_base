//改组件专门用于创建整个应用的路由器
import vueRouter from 'vue-router'

//引入组件
import About from '../pages/About.vue'
import Home from '../pages/Home.vue'
import News from '../pages/News.vue'
import Message from '../pages/Message.vue'
import Detail from '../pages/Detail.vue'

export default new vueRouter({
    routes:[
        {
            path:'/about',
            component:About
        },
        {
            path:'/home',
            component:Home,
            children:[
                {
                    // 命名路由，可以直接通过这么name属性值直接找到这条路由
                    name:"news",
                    path:'news',
                    component:News
                },
                {
                    path:'message',
                    component:Message,
                    children:[
                        {
                            name:'zf',
                            // 指定params参数
                            path:'detail/:id/:title',
                            component:Detail,

                            //props的三种写法
                            //第一种写法，值为对象，改对象中所有的key-value都会以props的形式传给Detail组件
                            // props:{a:1,b:'hello'},

                            //第二种写法，值为布尔值，若布尔值为真，就会吧改路由组件收到的所有params参数，以props的形式传给Detail组件
                            // props:true,

                            //第三种写法，值为函数
                            props($route){
                                return {
                                    id:$route.params.id,
                                    title:$route.params.title,
                                    a:1,
                                    b:'hello'
                                }                            
                            }

                        }
                    ]
                }
            ]
        }
    ]

    
})