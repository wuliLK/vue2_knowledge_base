//引入Vue
import Vue from 'vue'
//引入App
import App from './App.vue'

//完整引入
// //引入Element ui
// import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
// //使用element UI 组件
// Vue.use(ElementUI);

//按需引入

import {Button, Select, Row} from 'element-ui'
Vue.component(Button.name, Button);
Vue.component(Select.name, Select);
Vue.component(Row.name, Row);
//关闭Vue的生产提示
Vue.config.productionTip = false






//创建vm
new Vue({
	el:'#app',
	render: h => h(App),
})