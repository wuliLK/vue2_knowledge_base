## 初始Vue
- 容器和示例之间的关系是一对一的，一个容器只能被一个Vue示例接管，一个Vue示例只能绑定到一个容器中
- 容器中的代码依然要符合html的语法规范，只不过里面加入了一些vue的语法
- 容器中的代码被称为【vue模板】
- {{xxx}}是一个插值语法，xxx是js表达式
- 真实开发中只有一个vue实例，并且会配置着组件一起使用



## 模板语法
- 插值语法
    1. 功能: 用于解析标签体内容
    2. 语法: {{xxx}} ，xxxx 会作为js 表达式解析
- 指令语法
    1. 功能: 解析标签属性、解析标签体内容、绑定事件
    2. 举例：v-bind:href = 'xxxx' ，xxxx 会作为js 表达式被解析
    3. 说明：Vue 中有有很多的指令，此处只是用v-bind: 举个例子，`v-bind:`可以简写为`:`
    4. Vue中的指令都是以`v-`开头的


## 数据绑定
- 单向数据绑定
    1. Vue实例中的数据变化，容器中的数据会跟着变化；但反过来不行
- 双向数据绑定
    1. Vue实例中的数据变化，容器中的数据会跟着变化
    2. 反过来，容器中的数据变化，Vue实例中的数据也会跟着变化
    3. 双向数据绑定一般只应用于表单类元素（如input标签、select标签中的属性）    
    4. `v-model:value`可以简写为`v-model`

***注意对于标签属性值的绑定和标签之间的内容的绑定的不同*

- 对于标签属性的绑定是直接用引号
- 对于标签之间内容的绑定是使用双花括号`{{}}`

```html
姓: <input type="text" v-model="lastname"><!--标签属性的绑定 -->
<h2>name: {{name}}</h2>  <!-- 标签之间内容的绑定-->

```


## el与data的两种写法

- el的两种写法
    - 在new Vue()对象的时候，直接配置好
    - 在newVue()对象之后，通过`vm.$mount("#root")`来执行el的值

- data的两种写法
    - 对象式，`data:{name:"Roy"}`
    - 函数式，`data:function(){return {name:"Roy"}}`或者`data(){return {name:"Roy"}}`

**data的函数式写法时，一定不要使用箭头函数，一旦使用箭头函数，this就不在是Vue实例了**


## MVVM模型
- M: 代表模型model，相当于data中的数据
- V: 代表视图View，相当于模板代码
- VM: 代表视图模型ViewModel，相当于Vue实例

1. data身上的所有属性最终都出现在VM 身上
2. vm身上所有的属性以及vm原型上所有的属性，在vm模板中都可以直接使用 hff


## 数据代理
1. Vue中的数据代理：
   - 通过vm对象来代理data对象中的属性的操作（读、写）
2. Vue中的数据代理的害处：
    - 更加方便的操作data中的数据
3. 基本原理：
    - 通过Object.defineProperty()吧data对象中所有的属性添加到vm上。为每一个添加到vm
    上的属性，都指定一个gettter/setter。在getter/setter内部去操作（读/写）data中对应的属性


## 事件的处理
1. 使用`v-on:xxx="callback"`或者`@xxx="callback"`绑定事件，其中xxx是事件名
2. 事件的回调需要配置在methods中，最终会在vm上
3. methods中配置的函数，不要使用箭头函数，否则this就不是vm了
4. methods中配置的函数，都是被Vue所管理的函数，this的指向是vm或组件实例对象
5. @click="demo"和@click="demo($event)"效果一致，且后者在传其他参数时，even对象不会丢失。


## 键盘事件
1. Vue中常见按键的别名
   - 回车==》enter
   - 删除==》delete(包括delete键和backspace键)
   - 退出==》esc
   - 空格==》space
   - 换行==》tab（特殊，必须配合keydown使用）
   - 上==》up
   - 下==》down
   - 左==>left
   - 右==>right

2. Vue未提供别名的按键，可以使用按键原始的key值去绑定，但注意要转为kebab-case(短横线命名)，如`caps-lock`
3. 系统修饰键（用法特殊）， ctrl, alt, shift, meta
    - 配合keyup使用时，按下按键的同时，再按下其他键，随后释放其他键，事件才能被触发
    - 配合keydown使用时，直接触发

## 事件修饰符
```html
    <div id="root">
        <!-- prevent阻止默认事件 -->
        <a href="https://www.baidu.com" @click.prevent="showInfo">百度</a>

        <!-- 阻止事件冒泡 -->
        <div class="demo1" @click="showInfo">
            <button @click.stop="showInfo">点我提示信息</button>
        </div>
        <!-- 事件只执行一次 -->
        <button @click.once="showInfo">点我提示信息</button><br>

        <!-- 事件可以连续调用 -->
        <a href="https://www.baidu.com" @click.stop.prevent="showInfo">点我提示信息</a>

        <!--  -->
    </div>
```


## 计算属性

*插值语法也可以调用函数，但是在调用函数的时候，一定要带上括号*
```html
<!-- 下面是通过插值语法调用methods中的showname()函数，不能省略调括号 -->
姓名<span>{{showname()}}</span>
```

1. 定义:要用的属性不存在,要通过已有属性计算得来。
2. 原理:底层借助了objcet.defineproperty方法提供的getter和setter.
3. get函数什么时候执行?<br>
    (1).初次读取时会执行一次。<br>
    (2).当依赖的数据发生改变时会被再次调用。
4. 优势:与methods实现相比，内部有缓存机制(复用），效率更高，调试方便。
5. 备注:<br>
    1.计算属性最终会出现在vm上，直接读取使用即可。<br>
    2.如果计算属性要被修改，那必须写set函数去响应修改，且set中要引起计算时依赖的数据发生变化<br>


## 属性监视
- 方法一，使用watch配置属性直接在创建vm对象时定义好
- 方法二，vm.$watch()

**开启深度监视**
- deep:true



```javascript
// Vue默认开启的浅层数据监视

//监视todoList数组
watch:{
    todoList(){//这样的监视是一个浅层监视，只能监视到数组本身的变化（如数组长度的变化）,不能监视到数组内部的对象的变化
        console.log("todoList数组被修改了")
    }
}

//想要开启深度监视，则不能使用watch的简写方式
watch:{
    todoList:{
        deep:true
        handler(newValue,oldValue){
            console.log("todoList数组中的对象发生了修改")
        }
    }
}
```




## computed和watch的区别
1. computed可以完成的功能，watch都可以完成
2. watch能够完成的功能，computed不一定能够完成，如watch可以进行异步操作

**两个重要的原则**
- 所有被Vue管理的函数，最好写成普通的函数，这样this的指向才是vm或者组件实例对象
- 所有不被Vue管理的函数（如，定时器函数、ajax回调函数、Promise的回调函数），最好写成箭头函数，这样this的指向才能是vm或者组件实例对象



## 条件渲染

1. v-if的写法
   - v-if="表达式"
   - v-else-if="表达式"
   - v-else="表达式"，但是v-else后面接的表达式没有判断的作用。

适用于：切换频率较低的场景
特点：如果表达式为false, 则不展示相关元素，直接移除
注意：v-if和v-else-if和v-else可以一起使用，但是要求结构不能被打断



2. v-show的写法
   - v-show="表达式"
   适用于：切换频率较高的场景
   特点：如果表达式为false，则相关的元素不会被删除，只是使用样式将其隐藏

3. 备注，使用v-if时，元素不一定能够被获取，使用v-show时，元素一定能够被获取
4. template标签只能和v-if配合使用，不能配合v-else使用
5. template不会改变DOM结构，而div会改变DOM结构



## 面试题：react、vue中的key有什么作用
1. 虚拟DOM中key的作用：
   - key是虚拟DOM对象的表示，当数据发生变化时，Vue会根据【新数据】生成【新的虚拟DOM】，
   - 随后Vue进行【新的虚拟DOM】与【旧的虚拟DOM】的差异比较，比较规则如下：

2. 对比规则：
   - 旧虚拟DOM 中找到了与新虚拟DOM相同的key：
     - 若虚拟DOM中内容没变，则直接使用之前真实的DOM！
     - 若虚拟DOM中内容变了，则生成新的真实的DOM，随后替换掉页面中之前的真实的DOM

   - 旧虚拟DOM中未找到与新虚拟DOM相同的key
     - 创建新的真实DOM，随后渲染到页面。
3. 用index作为key可能会引发的问题：
   - 若对数据进行：逆序添加、逆序删除等破坏顺序的操作，会产生没有必要的真实DOM 更新==》界面效果没问题，但效率低。
   - 如果结构中还包含输入类的DOM，则会产生错误DOM更新==》界面有问题
4. 开发中如何选择key？：
   - 最好使用每条数据的唯一标识作为key，比如id、手机号、身份证号、学号等唯一值
   - 如果不存在对数据的逆序添加，逆序删除等破坏顺序操作，仅用于渲染列表用于展示，使用index作为key是没有问题的。


## Vue是如何检测数据的改变的

### 响应式：数据一旦改变，页面也立马跟着改变

要想以后添加的属性也能响应式，需要通过下面两个API
- Vue.set(target,attribute/index,value)
- vm.$set(target,attribute/index,value)

**set函数的第一个参数不能是vm或者vm.data**
**对于数组中的每一个元素，Vue是没有为这些元素添加响应式的**
- 所以直通过数组索引来修改元素，是没有响应式的，即虽然值修改了，但是页面不会改变
- 但是如果数组中的元素是一个对象，则这个对象中的属性是响应式的，只是这个对象不是响应式的

**如何让修改数组中的某个元素时，是有响应式的呢？----需要通过下面操作数组的几个API**
- push()
- pop()
- shift()
- unshift()  *在数组前面添加一个元素*
- splice()   *splice(index,count,val),在index位置用val替换count个元素*
- sort()
- reverse()

**上面几个API已经不是数组对象上原生的API，而是Vue通过包装后的API**

**数据劫持其实就是一种数据代理**




## 收集表单数据

- 若：\<input type="text" />，则v-model收集的就是value值，用户输入的就是value值，
- 若：\<input type="radio" />，则v-model手机的是value值，且要给标签配置value值
- 若：\<input type="checkbox" />
  - 1. 没有配置input的value属性，name收集的就是checked（勾选 or 未勾选，是boolean 值）
  - 2. 配置input的value值：
    - （1）v-model的初始值是非数组，则收集的就是checked（勾选 or 未勾选，是boolean 值）
    - （2）v-model的初始值是数组，那么收集的就是value组成的数组

备注：
- v-model的三个修饰符
  - lazy：失去焦点再收集数据
  - number：输入字符串转为有效数组
  - trim：输入首尾空格过滤


## 过滤器

**定义**：对要显示的数据进行特定格式化后再显示（适用于一些简单逻辑的处理）
**语法**：
1. 注册过滤器：Vue.filter(name,callback)或者new Vue（filters:{}）
2. 使用过滤器：{{xxx | 过滤器名称}} 或 v-bind:属性="xxx | 过滤器名称"

**备注**
1. 过滤器也可以接收额外的参数，多个过滤器也可以串联
2. 并没有改变原本的数据，是产生新的对应的数据



## 内置指令

- v-bind:单向绑定解析表达式，可以简写为:xxx
- v-model:双向数据绑定
- v-for:遍历数组、对象、字符串
- v-on:绑定事件监听，可以简写为@
- v-if:条件渲染（动态控制节点是否存在）
- v-else:条件渲染（动态控制节点是否存在）
- v-show:条件渲染（动态控制节点是否展示，但节点还是存在的）

- v-text:
  - 作用：向其所在的节点中渲染文本内容
  - 与插值语法的区别：v-text会替换掉节点中的内容，{{xx}}则不会
- v-html:
  - 作用：向指定节点中渲染包含html结构的内容
  - 与插值语法的区别：
    - v-html会替换掉节点中所有的内容，{{xxx}}则不会
    - v-html可以识别html结构

  - 严重注意：v-html有安全性问题！！！！
    - 在网站上面动态渲染任意HTML是非常危险的，容易导致XSS攻击。
    - 一定要在可信的内容上使用v-html，永远不要在用户提交的内容上使用！

- v-cloak指令（没有值）
  - 本质是一个特殊的属性，Vue示例创建完毕并接管容器后，会删掉v-cloak属性
  - 使用css配合v-cloak可以解决网速慢时，页面展示出的{{xxx}}等未渲染时的页面。

- v-once指令
  - v-once所在的节点在初次动态渲染后，就被视为静态内容了。
  - 以后数据的改变不会引起v-once所在结构的更新了，可以用于优化性能
```html
<h2 v-once>初始化的n值是:{{n}}</h2>
<h2 >当前n值是:{{n}}</h2>
<button @click="n++">点我n+1</button>
```

- v-pre指令
  - 跳过其所在节点的编译过程
  - 可利用他跳过：没有使用指令语法、没有使用插值语法的节点，会加快编译。

**JS阻塞**
  - 当JS代码没有加载完，js后面的代码不会解析

**指令不能脱离元素存在的**
## 自定义指令
1. 定义语法
   - 局部指令：
      ```javascript
      new Vue({
        directives:{指令名:配置对象}
      })
      
      或者
      
      new Vue({
        directives{指令名:回调函数}
      })
      ```
    - 全局指令：
      ```javascript
      Vue.directive(指令名,配置对象)
      
      或者
      
      Vue.directive(指令名,回调函数)
      ```
2. 配置对象中常用的三个回调：

   - .bind:指令与元素成功绑定时调用
   - .inserted:指令所在元素被插入页面时调用。
   - .update:指令所在的模板结构被重新解析时调用
3. 备注：
   - 指令定义时不加`v-`，但是使用的时候需要加`v-`
   - 指令名如果是多个单词，要使用`kebab-case`命名方式，不要使用`camelCase`命名方式


## Vue生命周期

- 又名：生命周期回调函数、生命周期函数、生命周期钩子
- 是什么：Vue在关键时刻帮我们调用的一些特殊名称的函数
- 生命周期函数的名字不可更改，但函数的具体内容是程序员根据需求编写的
- 生命周期函数中的this指向是vm 或 组件实例对象

![](尚硅谷/解压后课件笔记/资料（含课件）/02_原理图/生命周期.png)


- 常用的生命周期钩子
  - mounted：发送ajax请求、启动定时器、绑定自定义事件、订阅消息等【初始化操作】
  - beforeDestroy：清楚定时器、解绑自定义事件、取消订阅消息等【收尾工作】

- 关于销毁Vue实例
  - 销毁后借助Vue开发者工具看不到任何信息
  - 销毁后自定义事件会失效，但是原生DOM事件依然有效
  - 一般不会在beforeDestroy中操作数据，因为几遍操作诗句，也不会再触发更新流程了


## 组件
定义：实现应用中**局部**功能**代码**和**资源**的**集合**

组件就是一块砖，哪里需要哪里搬

### 基本使用
1. Vue中使用组件的三大步骤：
   1. 定义组件（创建组件）
   2. 注册组件
   3. 使用组件（写组件标签）

2. 如何定义一个组件？
  - 使用Vue.extend(options)创建，其中options和new Vue(options)时传入的那个options几乎一样，但也有点区别，区别如下：
    - el不要写，为什么？----最终所有的组件都要经过一个vm的管理，由vm中的el决定服务哪个容器。
    - data必须携程函数，为什么？----避免组件被复用时，数据存在引用关系
    - 备注：使用template可以配置组件结构

3. 如何注册组件？
   - 局部注册：靠new Vue的时候传入components选项
   - 全局注册：使用Vue.component('组件名',组件)

4. 编写组件标签：
   - \<school>\</school>


**注意点：**
1. 关于组件名：
   1. 一个单词组成的：
      - 第一种写法（首字母小写）：school
      - 第二种写法（首字母大写）：School
   2. 多个单词组成：
      - 第一种写法（kebab-case命名）：my-school
      - 第二种写法（CamelCase命名）：MySchool（需要Vue脚手架支持）
   3. 备注：
        - 组件名尽可能回避HTML中已有的元素名称，例如h2、H2都不行。
        - 可以使用name配置项指定组件在开发者工具中呈现的名字

2. 关于组件标签：
   - 第一种写法：\<school>\</school>
   - 第二种写法：\<school/>
   - 备注:不适用脚手架时，\<school/>会导致后续组件不能渲染

3. 一个简写的方式：
```html
const school = Vue.extend(options)可简写为：const school = options
```

## VueComponent
1. school组件本质是一个名为VueComponent的构造函数，且不是程序员定义的，是Vue.extend生成的
2. 我们只需要写\<school></school>或者\<school/>，Vue解析时会帮助我们创建school组件的实例对象，即Vue帮我们执行new VueComponent(options)。
3. 特别注意：每次调用Vue.extend，返回的都是一个全新的VueComponent！！！！
4. 关于this的指向：
   - 组件配置中：data函数、methods中的函数、watch中的函数、computed中的函数，他们的this指向均是VueComponent实例对象
   - new Vue(options)配置中：data函数、methods中的函数、watch中的函数、computed中的函数 他们的this指向均是Vue实例对象

5. VueComponent的实例对象，以后简称vc（也可以称之为：组件实例对象）


## 内置关系（原型）
1. 一个重要的内置关系：`VueComponent.prototype.__proto__ == Vue.prototype`
2. 为什么要有这个关系：让组件实例对象（vc）可以访问到Vue原型上的属性、方法。

***注意：**每次调用Vue.extend时，返回的都是一个全新的VueComponent（按照Java那一套的话，相当于返回一个全新的类）
```javascript
const vc = Vue.extend({
   data(){
      a:10
   }
})

这里的vc其实相当于java中的一个类，具有prototype属性
而当使用的组件标签时，相当于java中创建了一个示例对象

<school></school>//这里相当于创建一个VueComponent的对象
```

- 实例的隐式原型永远指向自己缔造者的显示原型对象
- 原型链有点像java中的继承，让VueComponent也能访问到Vue原型中的属性、方法


 ## 单文件组件
 **ES6的模块化的三种暴露方式**
 - 单个暴露
  ```javascript
  export const school = Vue.extend({})
  ```
 - 统一暴露

```javascript
const school = Vue.extend({})
export {school}
```
 - 默认暴露

```javascript
const school = Vue.extend({})
export default school

或者
export default const school = Vue.extend({})

```

*默认暴露的引用方式为import xxx from xxxx*
*其他暴露的引用方式为import {xxx} from xxxx*



## 脚手架篇CLI
**关于不同版本的Vue:**
1. vue.js与vue.runtime.xxx.js的区别：
   - vue.js是完整版的Vue，包含：核心功能+膜版解析器
   - vue.runtime.xxx.js是运行班的Vue，只包含：核心功能；没有模板解析器

2. 因为vue.runtime.xxx.js没有模板解析器，所以不能使用template配置项，需要使用render函数接收到的createElement函数去指定具体内容。

**修改vue的默认配置**，参考：https://cli.vuejs.org/zh/config/#vue-config-js


### render函数的两种写法
**render函数的作用：将组件/HTML标签，放入容器之中**
- 简写方式
  - 自定义组件
  ```javascript
   render: h => h(App)
  ```
  - HTML标签
  ```javascript
   render: h => h('h2','你好啊')
  ```

- 完整写法
  - 自定义组件
  ```javascript
    render(createElement){
        return createElement(App)
    }
  ```
  - HTML标签
  ```javascript
      render(createElement){
        return createElement('h2','你好啊')
      }
  ```

```javascript
    render: h => h(App)
    // render(createElement){
    //     return createElement(App)
    // }
```



**vue.config.js**
- vue.config.js，是一个可选的配置文件，如果项目的 (和 package.json 同级的) 根目录中存在这个文件，那么它会被 @vue/cli-service 自动加载。

这个文件应该导出一个包含了选项的对象：
```javascript
// vue.config.js

/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
  // 选项...
}
```

*编译时，通过warnings*
- 默认情况下，编译如果有warings是不通过的
- 当修改了vue.config.js，项目需要重新启动，配置文件才会生效
```javascript
 module.exports = {
    devServer: {
      overlay: {
        //   通过下面的配置让warnings通过
        warnings: false,
        errors: true
      }
    }
  }
```

## ref属性
- 被用来给元素或子组件注册引用信息（id的替代者）
- 应用在HTML标签上获取的是真实DOM元素，应用在组件标签上是组件的实例对象
- 使用方式

```html
打标识：<h1 ref='xxx'>...</h1> 或者<School ref='xxx'></School>

获取：this.$refs.xxx
```

## 配置项props
- 功能：让组件接收外部传过来的数据
  - （1）传递数据：\<Demo name='xxx'>
  -  (2)接收数据：
     -  第一种方式，纯粹接收数据（数组）：props:['name']
     -  第二种方式，只限制数据类型：
  ```javascript
  props:{
     name:Number
  }
  ```
     -  第三种方式，（限制类型、限制必要性、指定默认值）

```javascript
props:{
   name:{
      type:String,//类型
      required:true,//必要性
      default:"老王"//默认值
   }
}
```

**备注：** props是只读的，Vue底层会检测你对props的修改，如果进行了修改，就会发出警告，若业务需求确实需要修改，那么请复制props的内容到data中一份，然后去修改data中的数据。



## mixin(混入)

- 功能：可以把多个组件共用的配置提取成一个混入的对象
- 使用方式：
  - 第一步，定义混合，例如：
  ```javascript
  export const hunhe = {
     data(){
        return {
           a:100
        }
     },
     methods:{
        showName(){
  
        }
     }
  }
  ```

  - 第二步，使用混入，例如：
    - (1)全局混入，Vue.mixin(xxx,xx)
    - (2)局部混入，mixins:[xxx,xx]

## 插件
- 功能：用于增强Vue
- 本质：包含insall方法的一个对象，install的第一个参数是Vue，第二个蚕食是插件使用者传递的数据
- 定义插件：

```javascript
对象.install = function(Vue,options){
   //添加全局过滤器
   Vue.filter(...)
   //添加全局指令
   Vue.directive(...)
   //配置全局混入
   Vue.mixin(options)
   //添加实例方法

   Vue.prototype.$myMethod = function(){}
   Vue.prototype.$property = xxx
}
```


## scoped样式
- 作用：让样式在局部生效，防止不同组件之间重名的样式发生冲突
- 写法：\<style scoped>\</style>



## TodoList案例

### 兄弟组件之间的通信

- **状态提升**：可以通过父组件进行，在父组件中定义一些数据项和方法，然后通过props配置项，传递给子组件。将共同父组件当做一个中介。





*v-model和checkbox一起使用时，有一点不一样*



**tips：**



```javascript
// checkbox，有一个事件叫做@change，当勾选状态改变时，会调用这个方法
<input type="checkbox" :checked="todo.done" @change="changeTodo(todo.id)"/>
    
// confirm("...")函数，根据用户的确认与否返回true 或者 false    
methods:{
    handleDelete(id){
        if(confirm('确定删除吗?')){
            ....
        }
    }
}


computed:{
    doneTotal(){
        //reduce可以用来计数，
        //reduce第二个参数代表着pre的初始值，pre代表着reduce第一个参数函数的返回值,cur代表着当前遍历的对象，
        //reduce函数执行完之后，会将最终的pre值返回。
        //reduce第一个函数执行的此时等于要遍历的容器的length
        const x = this.todos.reduce((pre,cur)=>{
            return pre + (cur.done ? 1 : 0)
        },0)
    }
}
```


## webStorage

1. 存储的内容大小一般支持5MB左右（不同浏览器可能还不一样）
2. 浏览器通过Window.sessionStorage和Window.localStorage函数来实现本地存储机制
3. 相关API,sessionStorage和localStorage的API是一样的：
   - `setItem('key','value')`，该方法接收一个键值对作为参数，会把键值对添加到存储中，如果键名存在，则更新其对应的值。
   - `getItem('key')`，该方法接收一个键名作为参数，返回键名对应的值
   - `removeItem('key')`，该方法接收一个键名作为参数，并把该键名对应的键值对从存储中删除
   - `clear()`，该方法会清空存储中所有的数据
4. SessionStorage存储的内容会随着浏览器窗口的额关闭而消失
5. LocalStorage存储的内容，需要手动清除才会消失
6. `getItem('key')`如果key对应的value获取不到，那么getItem返回的值是null
7. JSON.parse(null)的结果依然是null
8. JSON.stringify(对象)，将对象转换成json字符串的形式
9. JSON.parse(对象字符串)，将对象字符串解析成为一个对象



## 组件的自定义事件



1. 一种组件间通信的方式，适用于：<strong style="color:red">子组件 ===> 父组件</strong>

2. 使用场景：A是父组件，B是子组件，B想给A传数据，那么就要在A中给B绑定自定义事件（<span style="color:red">事件的回调在A中</span>）。

3. 绑定自定义事件：

   1. 第一种方式，在父组件中：```<Demo @atguigu="test"/>```  或 ```<Demo v-on:atguigu="test"/>```

   2. 第二种方式，在父组件中：

      ```js
      <Demo ref="demo"/>
      ......
      mounted(){
         this.$refs.xxx.$on('atguigu',this.test)
      }
      ```

   3. 若想让自定义事件只能触发一次，可以使用```once```修饰符，或```$once```方法。

4. 触发自定义事件：```this.$emit('atguigu',数据)```		

5. 解绑自定义事件```this.$off('atguigu')```，当需要解绑多个事件是，则需要通过数组的方式传入参数，`this.$off(['atguigu','zf'])`，若不传入参数，则解绑所有事件。

6. 组件上也可以绑定原生DOM事件，需要使用```native```修饰符。`<Demo @click.native="showName">`

7. 注意：通过```this.$refs.xxx.$on('atguigu',回调)```绑定自定义事件时，回调<span style="color:red">要么配置在methods中</span>，<span style="color:red">要么用箭头函数</span>，否则this指



- **事件是谁触发的，this的指向就是谁**
- **箭头函数没有this**
- **Vue默认给组件绑定的事件都是自定义事件，需要让Vue将其当成原生事件则需要使用`.native`修饰事件**









## 全局事件总线：任意组件间通信

1. 一种组件间通信的方式，适用于==任意组件间通信==

2. 安装全局事件总线 

   ```javascript
   new Vue({
       ......,
       beforeCreate(){
       	Vue.prototype.$bus = this;安装全局事件总线，$bus就是当前应用的vm
   	},
       ......,
   })
   ```

3. 使用事件总线：

   1. 接收数据，A组件想接受数据，则在A组件中给$bus绑定自定义事件，时间的==回调留在A组件自身==

   ```javascript
   methods:{
       demo(data){}
   }
   
   ...
   
   mounted(){
       this.$bus.$on('事件名称',this.demo)
   }
   ```

   2. 提供数据：`this.$bus.$emit('事件名称',数据)`

4. 最好在beforeDestory生命周期函数中，用`$off`去==解绑当前组件所用到的事件==





## 消息订阅与发布（pubsub）



1. 一种组件之间的通信方式，适用于任意组件间通信

2. 使用步骤：

   1. 安装pubsub:`npm i pubsub-js`
   2. 引入：`import pubsub from 'pubsub-js'`
   3. 接收数据：A组件想接收数据，则在A组件中订阅消息，订阅的==回调函数留在A组件自身==

   ```javascript
   
   methods(){
       demo(msgName,data){......}
   }
   ......
   mounted(){
       this.pid = pubsub.subscribe('xxx',this.demo)//订阅消息
   }
       
   beforeDestroy(){
       pubsub.unsubscribe(this.pid)//取消订阅
   }
   ```

   

   4. 提供数据：`pubsub.publish('xxx',数据)`
   5. 最好在`beforeDestory()`生命周期函数中，用`pubsub.unsubscribe(pid)`去取消订阅





**一个对象判断自己是否拥有某个属性**，如判断对象todo是否拥有属性isEdit，`todo.hasOwnProperty('isEdit')`

**setTimeout()和setInterval()的区别**

- setTimeout(表达式,延时时间)在执行时,是在载入后延迟指定时间后,去执行一次表达式,记住,**次数是一次**

- setInterval(表达式,交互时间)则不一样,它从载入后,**每隔指定的时间就执行一次表达式**，所以,完全是不一样的

****



## nextTick

1. 语法：```this.$nextTick(回调函数)```
2. 作用：在下一次 DOM 更新结束后执行其指定的回调。
3. 什么时候用：当改变数据后，要基于更新后的新DOM进行某些操作时，要在nextTick所指定的回调函数中执行。





## 跨域请求

说道**跨域**，先看下浏览器的**同源协议（Same-Origin-Policy）**，它是浏览器的**默认安全措施**，一般一个网址通常由 **protocol**，**domain**，**port** 三个部分所组成，根据SOP同源协议，如果一个网址只要**有一个部分的不符合**，便不能访问，这种行为就是跨域。



**解决跨域访问，常见的三种方法：**

- 修改响应头

  同源协议并不是禁止浏览器请求跨域服务器，而是浏览器拦截了跨域服务器的响应。所以可以通过修改跨域服务器的响应头，在响应头上面添加属性，

  - **access-control-allow-origin** –服务器允许的来源
  - **access-control-allow-methods** –服务器允许的方法的逗号分隔列表
  - **access-control-allow-headers** -服务器将允许的逗号分隔的头列表
  - **access-control-max-age** –告诉浏览器将对预检请求的响应缓存多长时间（以秒为单位）
  - [MDN Web文档](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#the_http_response_headers)中列出了可能的CORS响应标头的完整列表。

  

- jsonp

  - JSONP 是服务器与客户端跨源通信的常用方法之一。最大特点就是简单适用，兼容性好（兼容低版本IE），**缺点是只支持get请求，所以一般我们也不用它**。

  - **核心思想**：网页通过添加一个`<script>`元素，向服务器请求 JSON 数据，服务器收到请求后，将数据放在一个指定名字的回调函数的参数位置传回来。

    **前端实现：**

    ```html
    <script src="http://localhost:8080/map?callback=dosomething"></script>
    // 向服务器发出请求，该请求的查询字符串有一个callback参数，用来指定回调函数的名字
     
    // 处理服务器返回回调函数的数据
    <script type="text/javascript">
        function dosomething(res){
            // 处理获得的数据
            console.log(res)
        }
    </script>
    ```

    **后端实现：**

    ```java
    @RestController
    @Slf4j
    public class MapController {
        @RequestMapping(value = "/map")
        public String transfer(String callback) {
            log.info(callback);
            return callback + "('lakertest')";
        }
    }
    ```

    



- 反向代理

  浏览器和其他设备通信时，收同源协议的保护限制，但是两台服务器之间的通信是不受同源协议的限制的。因此可以设置一个**代理服务器**，且这个代理服务器的与浏览器是同源的，这样浏览器和代理服务器之间可以正常通信，然后代理服务器和目标资源服务器也可以通信，代理服务器代理所有浏览器向目标资源服务器的请求，然后转发所有目标资源服务器的响应。

  Vue中开启代理服务器：

  ```javascript
  //在vue.config.js文件中按照下面的两种方式之一配置一下，就可以解决跨越访问的问题了
  module.exports = {
  	//开启代理服务器（方式一）
  	/* devServer: {
      proxy: 'http://localhost:5000'
    }, */
  	//开启代理服务器（方式二）
  	devServer: {
      proxy: {
        '/atguigu': {
          target: 'http://localhost:5000',
  				pathRewrite:{'^/atguigu':''},
          // ws: true, //用于支持websocket
          // changeOrigin: true //用于控制请求头中的host值
        },
        '/demo': {
          target: 'http://localhost:5001',
  				pathRewrite:{'^/demo':''},
          // ws: true, //用于支持websocket
          // changeOrigin: true //用于控制请求头中的host值
        }
      }
    }
  }
  ```

  

## vue脚手架配置代理

### 方法一

​	在vue.config.js中添加如下配置：

```js
devServer:{
  proxy:"http://localhost:5000"
}
```

说明：

1. 优点：配置简单，请求资源时直接发给前端（8080）即可。
2. 缺点：不能配置多个代理，不能灵活的控制请求是否走代理。
3. 工作方式：若按照上述配置代理，当请求了前端不存在的资源时，那么该请求会转发给服务器 （优先匹配前端资源）

### 方法二

​	编写vue.config.js配置具体代理规则：

```js
module.exports = {
	devServer: {
      proxy: {
      '/api1': {// 匹配所有以 '/api1'开头的请求路径
        target: 'http://localhost:5000',// 代理目标的基础路径
        changeOrigin: true,
        pathRewrite: {'^/api1': ''}
      },
      '/api2': {// 匹配所有以 '/api2'开头的请求路径
        target: 'http://localhost:5001',// 代理目标的基础路径
        changeOrigin: true,
        pathRewrite: {'^/api2': ''}
      }
    }
  }
}
/*
   changeOrigin设置为true时，服务器收到的请求头中的host为：localhost:5000
   changeOrigin设置为false时，服务器收到的请求头中的host为：localhost:8080
   changeOrigin默认值为true
*/
```

说明：

1. 优点：可以配置多个代理，且可以灵活的控制请求是否走代理。
2. 缺点：配置略微繁琐，请求资源时必须加前缀。





## 插槽

1. 作用：让父组件可以向子组件指定位置插入html结构，也是一种组件间通信的方式，适用于==父组件向子组件传递数据==

2. 分类：默认插槽、具名插槽、作用域插槽

3. 使用方式：

   1. 默认插槽：

      ```vue
      父组件中：
              <Category>
                 <div>html结构1</div>
              </Category>
      子组件中：
              <template>
                  <div>
                     <!-- 定义插槽 -->
                     <slot>插槽默认内容...</slot>
                  </div>
              </template>
      ```

   2. 具名插槽：

      ```vue
      父组件中：
              <Category>
                  <template slot="center">
                    <div>html结构1</div>
                  </template>
      
                  <template v-slot:footer>
                     <div>html结构2</div>
                  </template>
              </Category>
      子组件中：
              <template>
                  <div>
                     <!-- 定义插槽 -->
                     <slot name="center">插槽默认内容...</slot>
                     <slot name="footer">插槽默认内容...</slot>
                  </div>
              </template>
      ```

   3. 作用域插槽：

      1. 理解：<span style="color:red">数据在组件的自身，但根据数据生成的结构需要组件的使用者来决定。</span>（games数据在Category组件中，但使用数据所遍历出来的结构由App组件决定），且**必须搭配template标签使用**

      2. 具体编码：

         ```vue
         父组件中：
         		<Category>
         			<template scope="scopeData">
         				<!-- 生成的是ul列表 -->
         				<ul>
         					<li v-for="g in scopeData.games" :key="g">{{g}}</li>
         				</ul>
         			</template>
         		</Category>
         
         		<Category>
         			<template slot-scope="scopeData">
         				<!-- 生成的是h4标题 -->
         				<h4 v-for="g in scopeData.games" :key="g">{{g}}</h4>
         			</template>
         		</Category>
         子组件中：
                 <template>
                     <div>
                         <slot :games="games"></slot>
                     </div>
                 </template>
         		
                 <script>
                     export default {
                         name:'Category',
                         props:['title'],
                         //数据在子组件自身
                         data() {
                             return {
                                 games:['红色警戒','穿越火线','劲舞团','超级玛丽']
                             }
                         },
                     }
                 </script>
         ```

   

   





## Vuex

### 1. 概念	

​	在Vue中实现集中式状态（数据）管理的一个Vue插件，对Vue应用多个组件的共享状态进行集中式管理（读/写），也是一种组件间通信方式，且适用于任意组件间通信。

### 2. 何时使用

对个组件需要共享数据时

### 3. 搭建vuex环境

1. 创建文件：`src/store/index.js`

```javascript
// 引入核心库
import Vue from 'vue'
// 引入Vuex
import Vuex from 'vuex'
//应用插件
Vue.use(Vuex)

//准备actions对象--响应组件中用户的动作
const actions = {
    ...
}

//准备mutations对象--修改state中的数据
const mutations = {
	...
}

//准备state对象--保存具体数据
const state = {
	...
}

//创建并暴露store
export default new Vuex.store({
	actions,
	mutations,
	state
})
```



2. 在`main.js`中创建vm时传入store配置项

```javascript
....
//引入store
import store from './store/index.js'
....

//创建vm
new Vue({
    el:'#app',
    render h => h(App),
    store
})
```



### 4. 基本使用

1. 初始化数据、配置actions、配置mutations、操作文件store.js

```js
//引入Vue核心库
import Vue from 'vue'

//引入Vuex插件
import Vuex from 'vuex'

//引用Vuex插件
Vue.use(Vuex)

const actions = {
    jia(context,value){
        //console.log('actions中的jia被调用了',context,value)
        context.commit('JIA',value)
    }
}

const mutations = {
    JIA(state,value){
        //console.log('mutations中的JIA方法被调用了',state,value)
        state.sum += value
    }
}

//初始化数据
const state = {
    sum:0
}

//创建并暴露store
export default new Vuex.Store({
    actions,
    mutations,
    state
})
```



2. 组件中读取vuex中的数据：`$store.state.sum`
3. 组件中修改vuex中的数据：`$store.dispatch('actions中的方法名',数据)`或者`$store.commit('mutations中的方法名',数据)`

**备注：** 若没有网络请求或其他业务逻辑，组件中也可以越过actions，即不写dispatch，直接编写commit



### 5. getters的使用

1. **概念：**当state中的数据需要经过加工后再使用时，可以使用getters加工。
2. 在`store.js`中追加`getters`配置

```js
...
const getters = {
    bigSum(state){
        return state.sum * 10
    }
}

//创建并暴露store
export default new Vuex.Store({
    ...,
    getters,
    ...
})
```

3. 组件中读取数据：`$store.getters.bigSum`



### 6. 四个map方法的使用

1. **mapState方法：**用于帮助我们映射`state`中数据为计算属性

```js
//引入mapState方法
import {mapState} from 'vuex'
...
computed:{
     //借助mapState生成计算属性：sum、school、subject（对象写法）
    ...mapState({sum:'sum',school:'school',subject:'subject'}),
        
         //借助mapState生成计算属性：sum、school、subject（数组写法）
    ...mapState(['sum','school','subject'])//仅限于对象写法中的键值对相同时，才能使用数组写法
}
```

2. **mapGetters方法：**用于帮助我们映射`getters`中的数据为计算属性

```js
// 引入mapGetters
import {mapGetters} from 'vuex'

...
computed:{
     //借助mapGetters生成计算属性：bigSum（对象写法）
    ...mapGetters({bigSum:'bigSum'}),
     //借助mapGetters生成计算属性：bigSum（数组写法）
    ...mapGetters(['bigSum'])
}
```

3. **mapActions方法：**用于帮助我们生成与`actions`对话的方法，即：`$store.dispatch(xxx)`的函数

```js
//引入mapActions方法
import {mapActions} from 'vuex'
...
methods:{
    //靠mapActions生成：incrementOdd、incrementWait（对象形式）
    ...mapActions({incrementOdd:'jiaOdd',incrementWait:'jiaWait'})

    //靠mapActions生成：incrementOdd、incrementWait（数组形式）
    ...mapActions(['jiaOdd','jiaWait'])
}
```

4. **mapMutations方法：**用于帮助我们生成与`mutations`对话的方法，即包含`$store.commit(xxx)`的函数

```js
//引入mapMutations方法
import {mapMutations} from 'vuex'
...
methods:{
    //靠mapActions生成：increment、decrement（对象形式）
    ...mapMutations({increment:'JIA',decrement:'JIAN'}),
    
    //靠mapMutations生成：JIA、JIAN（对象形式）
    ...mapMutations(['JIA','JIAN']),
}
```

**备注：**mapActions与mapMutations使用时，若需要传递参数需要：在模板中绑定事件时传递好参数，否则参数是事件对象。

### 7. 模块化+命名空间

1. 目的：让代码更好维护，让多种数据分类更加明确。

2. 修改```store.js```

   ```javascript
   const countAbout = {
     namespaced:true,//开启命名空间
     state:{x:1},
     mutations: { ... },
     actions: { ... },
     getters: {
       bigSum(state){
          return state.sum * 10
       }
     }
   }
   
   const personAbout = {
     namespaced:true,//开启命名空间
     state:{ ... },
     mutations: { ... },
     actions: { ... }
   }
   
   const store = new Vuex.Store({
     modules: {
       countAbout,
       personAbout
     }
   })
   ```

3. 开启命名空间后，组件中读取state数据：

   ```js
   //方式一：自己直接读取
   this.$store.state.personAbout.list
   //方式二：借助mapState读取：
   ...mapState('countAbout',['sum','school','subject']),
   ```

4. 开启命名空间后，组件中读取getters数据：

   ```js
   //方式一：自己直接读取
   this.$store.getters['personAbout/firstPersonName']
   //方式二：借助mapGetters读取：
   ...mapGetters('countAbout',['bigSum'])
   ```

5. 开启命名空间后，组件中调用dispatch

   ```js
   //方式一：自己直接dispatch
   this.$store.dispatch('personAbout/addPersonWang',person)
   //方式二：借助mapActions：
   ...mapActions('countAbout',{incrementOdd:'jiaOdd',incrementWait:'jiaWait'})
   ```

6. 开启命名空间后，组件中调用commit

   ```js
   //方式一：自己直接commit
   this.$store.commit('personAbout/ADD_PERSON',person)
   //方式二：借助mapMutations：
   ...mapMutations('countAbout',{increment:'JIA',decrement:'JIAN'}),
   ```

 

## 路由

1. 理解：一个路由（route）就是一组映射冠以（key-value），多个路由需要路由器（router）进行管理
2. 前端路由：key是路径，value是组件

### 1. 基本使用

1. 安装vue-router，命令：`npm i vue-router`
2. 编写router的配置项

```js
//引入VueRouter
import VueRouter from 'vue-router'
//引入路由组件
import About from '../components/About'
import Home from '../components/Home'

//创建router实例对象，去管理一组一组的路由规则
const router = new VueRouter({
    routes:[
        {
            path:'/about', //父级路由路径前面一定要加“/”
            component:About
        },
        {
            path:'/home',
            component:Home
        }
    ]
})

//暴露router
export default router
```

3. 应用vue-router插件

```js
//引入Vue
import Vue from 'vue'
//引入App
import App from './App.vue'
//关闭Vue的生产提示
Vue.config.productionTip = false

//应用vue-router插件
import vueRouter from 'vue-router'
import router from './router'

Vue.use(vueRouter)



//创建vm
new Vue({
	el:'#app',
	render: h => h(App),
	router:router//需要加上这个配置项
})
```

4. 实现切换（active-class可配置高亮样式）

```vue
<router-link active-class='active' to:'/about'></router-link>
```

5. 指定展示位置

```vue
<router-view></router-view>
```

### 2. 几个注意点

- 路由组件通常存放在`pages`文件夹中，一般组件通常存放在`components`文件夹。
- 路由器组件通常存放在`router`文件夹中，命名为index.js时，import时可以省略文件名
- 通过切换，“隐藏”了的路由组件，默认是被销毁的，需要的时候再去挂载
- 每个组件都有自己的`$route`属性，里面存放着自己的路由信息
- 整个应用只有一个`$router`，可以通过组件的`$router`属性获取到

### 3. 多级路由

1. 配置路由规则，使用`children`配置项

```js
routes:[
    {
        path:'/about',
        component:About
    },
    {
        path:'/home',
        component:Home,
        children:[//通过children配置子级路由
            {
                path:'news',//子级路由的路径前面一定不要加“/”
                component:News
            },
            {
                path:'message',
                component:Message
            }
        ]
    }
]
```

2. 跳转（要写完整路径）

```vue
<router-link active-class='active' to="/home/news">News</router-link>
```



### 4. 路由的query参数

1. 传递参数

```vue
<!-- 跳转并携带query参数，to的字符串写法 -->
<router-link :to="/home/message/detail?id=666&title=你好"></router-link>

<!-- 跳转并携带query参数，to的对象写法 -->
<router-link :to="{
                      path:'/home/message/detail',
                      query:{
                        id:666,
                        title:'你好'
                      }
                  }">跳转</router-link>
```

2. 接收参数

```vue
<!-- 当前是Detail组件-->
<h2>{{$route.query.id}}</h2>
<h2>{{$route.query.title}}</h2>

```



### 5. 命名路由

1. 作用：可以简化路由的跳转

2. 如何使用

   1. 给路由命名

   ```js
   //引入vue-router插件
   import VueRouter from 'vue-router'
   
   //引入路由组件
   import Demo from '../pages/Demo.vue'
   import Test from '../pages/Test.vue'
   import Hello from '../pages/Hello.vue'
   
   export default new VueRouter({
       routes:[
           {
               path:'/demo',
               component:Demo,
               children:[
                   {
                       path:'test',
                       component:Test,
                       children:[
                           {
                               name:'hello' //给路由命名
                               path:'welcome',
                               component:Hello,
                           }
                       ]
                   }
               ]
           }
       ]
   })
   ```

   2. 简化跳转

   ```vue
   <!--简化前，需要写完整的路径 -->
   <router-link to="/demo/test/welcome">跳转</router-link>
   
   <!--简化后，直接通过名字跳转 -->
   <router-link :to="{name:'hello'}">跳转</router-link>
   
   <!--简化写法配合传递参数 -->
   <router-link 
   	:to="{
   		name:'hello',
   		query:{
   		   id:666,
               title:'你好'
   		}
   	}"
   >跳转</router-link>
   ```

   

### 6. 路由的params参数

1. 配置路由，声明就收params参数

```js
import VueRouter from 'vue-router'

export default new VueRouter({
    routes:[
        {
            path:'/home',
            component:Home,
            children:[
                {
                    path:'news',
                    component:News
                },
                {
                    component:Message,
                    children:[
                        {
                            name:'xiangqing',
                            path:'detail/:id/:title', //使用占位符声明接收params参数
                            component:Detail
                        }
                    ]
                }
            ]
        }        
    ]
})

```

2. 传递参数

```vue
<!-- 跳转并携带params参数，to的字符串写法 -->
<router-link :to="/home/message/detail/666/你好">跳转</router-link>
				
<!-- 跳转并携带params参数，to的对象写法 -->
<router-link 
	:to="{
		name:'xiangqing',
		params:{
		   id:666,
            title:'你好'
		}
	}"
>跳转</router-link>
```

**注意：**路由携带params参数时，若使用to对象的写法，则不能使用path配置项，必须使用name配置！！！



3. 接收参数

```vue
<h2>{{$route.params.id}}</h2>
<h2>{{$route.params.title}}</h2>
```

### 7. 路由的props配置

作用：让路由组件更方便的收到参数

```js
{
	name:'xiangqing',
	path:'detail/:id',
	component:Detail,

	//第一种写法：props值为对象，该对象中所有的key-value的组合最终都会通过props传给Detail组件
	// props:{a:900}

	//第二种写法：props值为布尔值，布尔值为true，则把路由收到的所有params参数通过props传给Detail组件
	// props:true
	
	//第三种写法：props值为函数，该函数返回的对象中每一组key-value都会通过props传给Detail组件
        //此时，函数传递进入的参数是相当于是this.$route
	props(route){
		return {
			id:route.query.id,
			title:route.query.title
		}
	}
}
```

### 8. `<router-link>的replace属性`

1. 作用：控制路由跳转是操作浏览器在当前会话窗口的历史记录的模式
2. 浏览器的会话历史记录有两种写入方式：分别是`push`和`replace`，`push`是追加历史记录，`replace`是替换当前记录。路由跳转时候默认为`push`
3. 如何开启`replace`模式：`<router-link replace ......>News</router-link>`

### 9. 编程式路由导航

1. 作用：不借助`<router-link>`实现路由跳转，让路由跳转更加灵活
2. 具体编码：

```js
//$router的两个API
this.$router.push({
	name:'xiangqing',
    params:{
        id:xxx,
        title:xxx
    }
})

this.$router.replace({
	name:'xiangqing',
    params:{
        id:xxx,
        title:xxx
    }
})
this.$router.forward() //前进
this.$router.back() //后退
this.$router.go() //可前进也可后退
```

### 10. 缓存路由组件

1. 作用：让不展示的路由组件保持挂载，不被销毁。常用来缓存用户在表单中输入的内容
2. 具体编码：

```vue
//不加参数，会让所有的路由组件都缓存
<keep-alive> 
    <router-view></router-view>
</keep-alive>

//只有一个路由组件需要缓存，则以字符串的形式传入include属性
<keep-alive include="News"> 
    <router-view></router-view>
</keep-alive>

//有多个路由组件需要缓存，则按照下面的方式传入include属性
<keep-alive :include="['News','Message']"> 
    <router-view></router-view>
</keep-alive>
```

**注意：**`include`属性传入的都是组件的名称，即组件中`name`属性值



### 11. 两个新的生命周期钩子

1. 作用：路由组件所独有的两个钩子，用于捕获路由组件的激活状态
2. 具体名字
   1. `activated`路由组件被激活时触发
   2. `deactivated`路由组件失活时触发
3. 举例：

```vue
<template>
	<ul>
		<li :style="{opacity}">欢迎学习Vue</li>
		<li>news001<input type="text"></li>
		<li>news002<input type="text"></li>
		<li>news003<input type="text"></li>
	</ul>
</template>

<script>
	export default {
		name:'News',
		data(){
			return{
				opacity:1
			}
		},
		beforeDestroy() {
			console.log('News组件即将被销毁')
		},
//News组件被激活了
		activated() {
			console.log('News组件被激活了')
			this.timer = setInterval(() => {
				console.log('@')
				this.opacity -= 0.01
				if(this.opacity <= 0) this.opacity = 1
			},16)
		},
//News组件失活了
		deactivated() {
			console.log('News组件失活了')
			clearInterval(this.timer)
		},
	}
</script>
```

### 12.路由守卫

1. 作用：对路由进行权限控制

2. 分类：全局守卫、独享守卫、组件内守卫

3. 全局守卫:

   ```js
   //改组件专门用于创建整个应用的路由器
   import vueRouter from 'vue-router'
   
   //引入组件
   import About from '../pages/About.vue'
   import Home from '../pages/Home.vue'
   import News from '../pages/News.vue'
   import Message from '../pages/Message.vue'
   import Detail from '../pages/Detail.vue'
   
   const router = new vueRouter({
       routes:[
           {
               path:'/about',
               component:About,
               meta:{title:'关于'}
           },
           {
               path:'/home',
               component:Home,
               meta:{title:"主页"},
               children:[
                   {
                       // 命名路由，可以直接通过这么name属性值直接找到这条路由
                       name:"news",
                       path:'news',
                       component:News,
                       meta:{isAuth:true,title:'新闻'}
                   },
                   {
                       path:'message',
                       component:Message,
                       meta:{isAuth:true,title:'消息'},
                       children:[
                           {
                               name:'zf',
                               // 指定params参数
                               path:'detail/:id/:title',
                               meta:{isAuth:true,title:"详情"},
                               component:Detail,
                               props($route){
                                   return {
                                       id:$route.params.id,
                                       title:$route.params.title,
                                       a:1,
                                       b:'hello'
                                   }                            
                               }
   
                           }
                       ]
                   }
               ]
           }
       ]
   
       
   })
   //全局前置路由守卫————初始化的时候被调用、每次路由切换之前被调用
   router.beforeEach((to,from,next)=>{
       console.log('前置路由守卫',to,from)
       if(to.meta.isAuth){//判断是否需要鉴权
           if(localStorage.getItem('school') === '华南理工大学'){
               next()
           }else{
               alert('学校名称不对，无权查看')
           }
       }else{
           next()
       }
   })
   
   //全局后置路由守卫————初始化的时候被调用、每次路由切换之后被调用
   router.afterEach((to,from)=>{
       console.log('后置路由守卫',to,from)
       document.title = to.meta.title || 'scut'
   })
   
   
   export default router
   ```

4. 独享守卫:

   ```js
   //改组件专门用于创建整个应用的路由器
   import vueRouter from 'vue-router'
   
   //引入组件
   import About from '../pages/About.vue'
   import Home from '../pages/Home.vue'
   import News from '../pages/News.vue'
   import Message from '../pages/Message.vue'
   import Detail from '../pages/Detail.vue'
   
   const router = new vueRouter({
       routes:[
           {
               path:'/about',
               component:About,
               meta:{title:'关于'}
           },
           {
               path:'/home',
               component:Home,
               meta:{title:"主页"},
               children:[
                   {
                       // 命名路由，可以直接通过这么name属性值直接找到这条路由
                       name:"news",
                       path:'news',
                       component:News,
                       meta:{isAuth:true,title:'新闻'},
                       //独享路由
                       beforeEnter : (to,from,next)=>{
                           console.log('独享路由守卫',to,from)
   						if(to.meta.isAuth){ //判断是否需要鉴权
   							if(localStorage.getItem('school')==='华南理工大学'){
   								next()
   							}else{
   								alert('学校名不对，无权限查看！')
   							}
   						}else{
   							next()
   						}
                       }
                   },
                   {
                       path:'message',
                       component:Message,
                       meta:{isAuth:true,title:'消息'},
                       children:[
                           {
                               name:'zf',
                               // 指定params参数
                               path:'detail/:id/:title',
                               meta:{isAuth:true,title:"详情"},
                               component:Detail,
                               props($route){
                                   return {
                                       id:$route.params.id,
                                       title:$route.params.title,
                                       a:1,
                                       b:'hello'
                                   }                            
                               }
   
                           }
                       ]
                   }
               ]
           }
       ]
   
       
   })
   
   //全局后置路由守卫————初始化的时候被调用、每次路由切换之后被调用
   router.afterEach((to,from)=>{
       console.log('后置路由守卫',to,from)
       document.title = to.meta.title || 'scut'
   })
   
   
   export default router
   ```

5. 组件内守卫：

   ```vue
   <template>
   	<h2>我是About的内容</h2>
   </template>
   
   <script>
   	export default {
   		name:'About',
           //组件内路由守卫，路由进入时调用
   		beforeRouteEnter (to, from, next) {
   			console.log('About--beforeRouteEnter',to,from)
   			if(to.meta.isAuth){ //判断是否需要鉴权
   				if(localStorage.getItem('school')==='华南理工大学'){
   					next()
   				}else{
   					alert('学校名不对，无权限查看！')
   				}
   			}else{
   				next()
   			}
   		},
           //组件内路由守卫，路由离开是调用
   		beforeRouteLeave (to, from, next) {
   			console.log('About--beforeRouteLeave',to,from)
   			next()
   		}
   
   	}
   </script>
   ```

### 13.路由器的两种工作模式



1. 对于一个url来说，什么是hash值？—— #及其后面的内容就是hash值。
2. hash值不会包含在 HTTP 请求中，即：hash值不会带给服务器。
3. hash模式：
   1. 地址中永远带着#号，不美观 。
   2. 若以后将地址通过第三方手机app分享，若app校验严格，则地址会被标记为不合法。
   3. 兼容性较好。
4. history模式：
   1. 地址干净，美观 。
   2. 兼容性和hash模式相比略差。
   3. 应用部署上线时需要后端人员支持，解决刷新页面服务端404的问题。

## UI组件库

1. 移动端常用UI组件库
   - [Vant](https://youzan.github.io/vant/)
   - [Cube UI](https://didi.github.io/cube-ui)
   - [Mint UI](http://mint-ui.github.io)

2. PC端常用的UI组件库
   - [Element UI](https://element.eleme.cn)
   - [IView UI](https://www.iviewui.co)



**UI组件库的使用**，以Element UI组件库为例

1. npm 安装

```bash
npm i element-ui
```

其他方式安装，参考官网

2. 完整引入

```js
//引入Vue
import Vue from 'vue'
//引入App
import App from './App.vue'

//完整引入
//引入Element ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
//使用element UI 组件
Vue.use(ElementUI);

//关闭Vue的生产提示
Vue.config.productionTip = false
//创建vm
new Vue({
	el:'#app',
	render: h => h(App),
})
```

**按需引入**

借助 [babel-plugin-component](https://github.com/QingWei-Li/babel-plugin-component)，我们可以只引入需要的组件，以达到减小项目体积的目的。

1. 安装 babel-plugin-component

```bash
npm install babel-plugin-component -D
```

2. 然后，将 `.babelrc` 修改为：

   **注意：**由于组件库可能和脚手架的版本不匹配，所以按照官网上去配置可能会报如下错

   `ERROR  Error: Cannot find module 'babel-preset-es2015'`

   所以按照如下方式修改`babel.config.js`文件

   ```bash
   module.exports = {
     presets: [
       '@vue/cli-plugin-babel/preset',
       ["@babel/preset-env", { "modules": false }]
     ],
     "plugins": [
       [
         "component",
         {
           "libraryName": "element-ui",
           "styleLibraryName": "theme-chalk"
         }
       ]
     ]
   }
   
   ```

3. 在`main.js`文件中按需引入

```js
//引入Vue
import Vue from 'vue'
//引入App
import App from './App.vue'

//按需引入
import {Button, Select, Row} from 'element-ui'
Vue.component(Button.name, Button);
Vue.component(Select.name, Select);
Vue.component(Row.name, Row);

//关闭Vue的生产提示
Vue.config.productionTip = false
//创建vm
new Vue({
	el:'#app',
	render: h => h(App),
})
```





















Element UI组件按需引入是需要修改`babel.config.js`文件的配置

```js
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',
    ["@babel/preset-env", { "modules": false }]
  ],
  "plugins": [
    [
      "component",
      {
        "libraryName": "element-ui",
        "styleLibraryName": "theme-chalk"
      }
    ]
  ]
}

```

