# Vue2_knowledge_base

#### 介绍
基于B站上尚硅谷的张天禹老师的Vue视频所做的笔记和代码

#### 使用说明

1.  01——19都是基于vue.js这个文件开发的，非脚手架的案例
2.  vue_test里面都是基于Vue2脚手架开发的案例
3.  尚硅谷这个目录是B站上张天禹老师的官方Vue笔记
4.  Vue笔记.md是我自己基于官方笔记所写的笔记



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
